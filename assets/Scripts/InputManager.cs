﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//
//		OPTIMIZED PARTLY!!!
//

public class InputManager : MonoBehaviour {
	
	public float turnAmount;
	public float scaleAmount;
	public float moveAmount;
	LevelScript script;
	RaycastHit hit;
	public float angle;
	public float relativeTurnRatePower;
	private bool gameIsOver;
	private bool aMenuIsOpen;
	
	public Vector3 startPos, deltaPos;
	public float touchTime, longClickTime, gameStartTime;
	public float speed;	
	bool allowScroll = false;
	
	
	public bool allowInputMenu, allowInputGame, allowGameStart, allowInputTutorial;
	
	// Use this for initialization
	void Start () {
		GameEventManager.GameStart += GameStart;
		GameEventManager.GameOver += GameOver;
		GameEventManager.MainMenuStart += MainMenuStart;
		GameEventManager.MainMenuOver += MainMenuOver;
		GameEventManager.TriggerGameOver();
		gameIsOver = true;
		allowGameStart = true;
	}
	
	// Update is called once per frame
	void Update () {
		// Wait for one second after the game is lost so the player doesn't immediately start a new game after losing
		if (!allowGameStart && gameIsOver){
			gameStartTime += Time.deltaTime;
			if (gameStartTime > 1){
				allowGameStart = true;
				gameStartTime = 0;
			}
		}
		
		// If the player is not in a menu, open a menu if Back key is pressed
		if (!allowInputMenu && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyUp(KeyCode.P))){
			GameEventManager.TriggerMainMenuStart();
		}
		
		// If the player not in a menu and is not scrolling, touchin the screen starts a new game
		if (Input.GetKeyDown(KeyCode.Mouse0) && !allowScroll && !allowInputMenu){
			if (gameIsOver && !allowInputMenu && allowGameStart){
				GameEventManager.TriggerGameStart();
			}
			
			// If the tutorial is completed, turn off the allowInputTutorial bool
			if (TutorialManager.tutorialCompleted){
				allowInputTutorial = false;
			} else {
				allowInputTutorial = true;
			}
			
			// Hit recognition for the levels
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, 100)){
				// If the hit object is a level continue
				if (hit.transform.gameObject.name.Contains("Level")){
					script = (LevelScript)hit.transform.GetComponent(typeof(LevelScript));
					// If that hit level is clickable, continue
					if (script.clickable){
						GameObjectManager.soundBlibObject.GetComponent<AudioSource>().Play();
						
						// Update the current score to the heigth at the time of the click
						GameObjectManager.hsScript.UpdateCurrentScore();
						script.timeToClick = 0;
								
						CalculateAngle(hit.point, hit.transform);
						var relativePoint = hit.transform.InverseTransformPoint(hit.point);
						
						AdjustAngle(relativePoint.z);
						// If tutorial is not completed yet, go here
						if (allowInputTutorial){
							if (TutorialManager.CheckStage() >= 2){
								SendInputRotate(relativePoint.x, 2);
								TutorialManager.SetStageBoolTrue(TutorialManager.CheckStage());
								SendInputGrow();	
								script.rotBool = true;
								script.time2 = 0;
							}
							if (TutorialManager.CheckStage() == 1){
								SendInputRotate(relativePoint.x, 1);
							}
							if (TutorialManager.CheckStage() == 0){
								SendInputRotate(relativePoint.x, 0);
							}
						}
						// If tutorial is completed, go here
						if (allowInputGame && !allowInputTutorial){
							SendInputRotate(relativePoint.x);
							SendInputGrow();
							script.rotBool = true;
							script.time2 = 0;
						}
						ChangeClickable();
						CreateNewLevel();
					}
				} else {
					return;
				}
			}
		}
		
		if (Input.touchCount == 1){
			Touch touch = Input.GetTouch(0);
			touchTime += Time.deltaTime;
			var lastBool = allowScroll;
			if (touchTime > longClickTime){
				allowScroll = true;
			}
			if (allowScroll && !lastBool){
				startPos = Input.mousePosition;
			}
			if (touch.phase != TouchPhase.Stationary && allowScroll){
				deltaPos = startPos - Input.mousePosition;
			}
		}
		if (Input.touchCount == 0){
			allowScroll = false;
			touchTime = 0;
		}
		if (allowScroll){
			if (Camera.main.transform.position.z >= 7 && Camera.main.transform.position.z <= GameObjectManager.lastLevel.transform.position.z + 8){
					Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z + deltaPos.y / speed), Time.deltaTime * 3);
			} else if(Camera.main.transform.position.z < 7){
					Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 7);
			} else {
				deltaPos = Vector3.zero;
				startPos = Vector3.zero;
			}
		}
	}
	
	private void CalculateAngle(Vector3 hitpoint, Transform hitobj){
		Vector3 hitpos = hitobj.transform.position;
		Vector3 hitforward = hitobj.transform.forward;
		angle = Vector3.Angle(new Vector3(hitpoint.x - hitpos.x, 0, hitpoint.z - hitpos.z), new Vector3(hitpos.x - (hitpos.x - hitforward.x), 0, hitpos.z - (hitpos.z - hitforward.z)));
	}
	
	private void AdjustAngle(float relativePoint){
		if (relativePoint < 0.0f){
			angle = 180 - angle;
		} else if (relativePoint > 0.0f){
		} else {
		}
	}
	
	private void SendInputRotate(float relativePoint, int completetutorialdirection = 2){
		// Right
		if (relativePoint < 0.0f && (completetutorialdirection == 2 || completetutorialdirection == 0)){
			if (completetutorialdirection == 0){
				TutorialManager.SetStageBoolTrue(TutorialManager.CheckStage());
				SendInputGrow();
				script.rotBool = true;
				script.time2 = 0;
			}
			script.startRot = hit.transform.localEulerAngles;
			script.endRot = new Vector3(0, script.startRot.y + ((-angle * script.relativeTurnRate) / turnAmount) * script.relativeTurnRate, 0);
		// Left
		} else if (relativePoint > 0.0f && (completetutorialdirection == 2 || completetutorialdirection == 1)){
			if (completetutorialdirection == 1){
				TutorialManager.SetStageBoolTrue(TutorialManager.CheckStage());
				SendInputGrow();
				script.rotBool = true;
				script.time2 = 0;
			}
			script.startRot = hit.transform.localEulerAngles;
			script.endRot = new Vector3(0, script.startRot.y + ((angle * script.relativeTurnRate) / turnAmount) * script.relativeTurnRate, 0);
		} else {
			return;
		}
	}
	
	private void SendInputGrow(){
		if (script.size <= 2){
			script.size++;
			script.sizeBool = true;
		}
	}
	
	private void ChangeClickable(){
		if (script.size == 4){
			script.clickable = false;
			var nextClickableLevel = GameObjectManager.objectList[script.index + 1];
			((LevelScript)nextClickableLevel.GetComponent(typeof(LevelScript))).clickable = true;
			CameraScript.target = nextClickableLevel;
		}
		if (hit.transform.gameObject == GameObjectManager.objectList.Last() && script.size != 3){
			script.ReverseAllowedSelection();
		}
	}
	
	private void CreateNewLevel(){
		if (script.size == 3){
			GameObjectManager.lastLevel = hit.transform.gameObject;
			GameObject newlevel = (GameObject)Instantiate(GameObjectManager.level, hit.transform.position - hit.transform.forward * 1.9f, Quaternion.Euler(hit.transform.eulerAngles));
			newlevel.transform.parent = GameObjectManager.lastLevel.transform;
			GameObjectManager.objectList.Add(newlevel);
			script.clickable = false;
			script.size++;
			UpdateRelativeTurnRate();
		}
	}
	
	public void UpdateRelativeTurnRate(){
		foreach (GameObject obj in GameObjectManager.objectList) {
			LevelScript ls = (LevelScript)obj.transform.GetComponent(typeof(LevelScript));
			if (ls != null){
				ls.t1 = GameObjectManager.objectList.Count - 1;
				ls.relativeTurnRate = Mathf.Pow((ls.index / ls.t1), relativeTurnRatePower / 100);
			}
			
		}
	}
	//Event Methods
	
	private void GameStart(){
		allowInputMenu = false;
		allowInputGame = true;
		gameIsOver = false;
		allowGameStart = false;
	}
	
	private void GameOver(){
		allowInputMenu = false;
		allowInputGame = false;
		gameIsOver = true;
	}
	
	private void MainMenuStart(){
		allowInputMenu = true;
		allowInputGame = false;
	}
	
	private void MainMenuOver(){
		allowInputMenu = false;
		allowInputGame = true;
	}
	
	// Distance etc
	
	public static float MouseDistanceToLine(Vector3 linePoint1, Vector3 linePoint2, RaycastHit hit){
 
		Camera currentCamera;
		Vector3 mousePosition;
 
		currentCamera = Camera.main;
		mousePosition = hit.point;
 
		Vector3 screenPos1 = currentCamera.WorldToScreenPoint(linePoint1);
		Vector3 screenPos2 = currentCamera.WorldToScreenPoint(linePoint2);
		Vector3 projectedPoint = ProjectPointOnLineSegment(screenPos1, screenPos2, mousePosition);
 
		//set z to zero
		projectedPoint = new Vector3(projectedPoint.x, projectedPoint.y, 0f);
 
		Vector3 vector = projectedPoint - mousePosition;
		return vector.magnitude;
	}
	
	public static Vector3 ProjectPointOnLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point){
 
		Vector3 vector = linePoint2 - linePoint1;
 
		Vector3 projectedPoint = ProjectPointOnLine(linePoint1, vector.normalized, point);
 
		int side = PointOnWhichSideOfLineSegment(linePoint1, linePoint2, projectedPoint);
 
		//The projected point is on the line segment
		if(side == 0){
 
			return projectedPoint;
		}
 
		if(side == 1){
 
			return linePoint1;
		}
 
		if(side == 2){
 
			return linePoint2;
		}
 
		//output is invalid
		return Vector3.zero;
	}
	
	public static Vector3 ProjectPointOnLine(Vector3 linePoint, Vector3 lineVec, Vector3 point){		
 
		//get vector from point on line to point in space
		Vector3 linePointToPoint = point - linePoint;
 
		float t = Vector3.Dot(linePointToPoint, lineVec);
 
		return linePoint + lineVec * t;
	}
	
	public static int PointOnWhichSideOfLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point){
 
		Vector3 lineVec = linePoint2 - linePoint1;
		Vector3 pointVec = point - linePoint1;
 
		float dot = Vector3.Dot(pointVec, lineVec);
 
		//point is on side of linePoint2, compared to linePoint1
		if(dot > 0){
 
			//point is on the line segment
			if(pointVec.magnitude <= lineVec.magnitude){
 
				return 0;
			}
 
			//point is not on the line segment and it is on the side of linePoint2
			else{
 
				return 2;
			}
		}
 
		//Point is not on side of linePoint2, compared to linePoint1.
		//Point is not on the line segment and it is on the side of linePoint1.
		else{
 
			return 1;
		}
	}
}
