﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//
//		OPTIMIZED PARTLY!!!
//

public class AchievementManager : MonoBehaviour {
	
	public static string[] achiNames = new string[9];
	private bool tutbool, count1bool, count2bool, count3bool;
	
	public GameObject achiPopUp1, achiPopUp2, achiPopUp3, achiPopUp4, achiPopUp5, achiPopUp6, achiPopUp7, achiPopUp8, achiPopUp9;
	
	float time;
	bool showPopup;
	GameObject popup;
	
	// Use this for initialization
	void Start () {
//		PlayerPrefs.DeleteAll();
		achiNames[0] = "Achievement1";
		achiNames[1] = "Achievement2";
		achiNames[2] = "Achievement3";
		achiNames[3] = "Achievement4";
		achiNames[4] = "Achievement5";
		achiNames[5] = "Achievement6";
		achiNames[6] = "Achievement7";
		achiNames[7] = "Achievement8";
		achiNames[8] = "Achievement9";
		UpdateAchievements();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (!CheckTutorialCompleted()){
			CheckLevelCount();
			CheckLevelHeights();
		}
		if (showPopup){
			time += Time.deltaTime;
			if (time > 4){
				Destroy(popup);
				showPopup = false;
				time = 0;
			}
		}
	}
	
	private bool CheckTutorialCompleted(){
		if (TutorialManager.tutorialCompleted && !tutbool){
			SetAchievementTrue(1);
			tutbool = true;
			return true;
		}
		return false;
	}
	
	private void CheckLevelCount(){
		if (GameObjectManager.objectList.Count != 0){
			int count = GameObjectManager.objectList.Count - 1;
			SetCountAchiTrue(count, count1bool, 15, 2);
			SetCountAchiTrue(count, count2bool, 30, 3);
			SetCountAchiTrue(count, count3bool, 50, 4);
		}
	}
	
	private void SetCountAchiTrue(int count, bool countbool, float amount, int achi){
		if (count == amount && !countbool){
			SetAchievementTrue(achi);
			countbool = true;
		}
	}
	
	private void CheckLevelHeights(){
		if (GameObjectManager.heightGUI != null){
			float height = GameObjectManager.heightGUI.transform.position.z;
			
			SetHeightAchiTrue(height, 20, 20.2f, 5);
			SetHeightAchiTrue(height, 40, 40.2f, 6);
			SetHeightAchiTrue(height, 60, 60.2f, 7);
			SetHeightAchiTrue(height, 80, 80.2f, 8);
			SetHeightAchiTrue(height, 120, 120.2f, 9);
		}
	}
	
	private void SetHeightAchiTrue(float height, float min, float max, int achi){
		if (height > min && height < max){
			SetAchievementTrue(achi);
		}
	}
	
	public void ShowAchievementPopUp(int achievement){
		showPopup = false;
		time = 0;
		if (popup != null){
			Destroy(popup);
		}
		switch (achievement) {
			case 1:
				popup = (GameObject)Instantiate(achiPopUp1);
			break;
			case 2:
				popup = (GameObject)Instantiate(achiPopUp2);
			break;
			case 3:
				popup = (GameObject)Instantiate(achiPopUp3);
			break;
			case 4:
				popup = (GameObject)Instantiate(achiPopUp4);
			break;
			case 5:
				popup = (GameObject)Instantiate(achiPopUp5);
			break;
			case 6:
				popup = (GameObject)Instantiate(achiPopUp6);
			break;
			case 7:
				popup = (GameObject)Instantiate(achiPopUp7);
			break;
			case 8:
				popup = (GameObject)Instantiate(achiPopUp8);
			break;
			case 9:
				popup = (GameObject)Instantiate(achiPopUp9);
			break;
		}
		GameObjectManager.soundAchiUnlockObject.GetComponent<AudioSource>().Play();
		showPopup = true;
		popup.transform.position = new Vector3(popup.transform.position.x, popup.transform.position.y, popup.transform.position.z + (Camera.main.transform.position.z - 7));
		popup.transform.parent = Camera.main.transform;
	}
	
	public void SetAchievementTrue(int achinumber){
		achinumber--;
		if (PlayerPrefs.GetInt(achiNames[achinumber]) != 1){
			string achi = achiNames[achinumber];
			PlayerPrefs.SetInt(achi, 1);
			ShowAchievementPopUp(achinumber + 1);
		}
	}
	
	public void UpdateAchievements(){
		for (int i = 0; i < achiNames.Length; i++) {
			string achiString = achiNames[i];
			var achiInt = PlayerPrefs.GetInt(achiString);
		}
	}
	
	public static int CheckSingleAchievement(int achievement){
		achievement--;
		string achiString = achiNames[achievement];
		var achiInt = PlayerPrefs.GetInt(achiString);
		return achiInt;
	}
}
