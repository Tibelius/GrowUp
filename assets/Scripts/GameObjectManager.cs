﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//
//		OPTIMIZED PARTLY!!!
//

public class GameObjectManager : MonoBehaviour {
	
	public static List<GameObject> objectList = new List<GameObject>();
	public GameObject _level, _start, _startLevel, _scoreGUI, _heightText1, _heightText2, _heightGUI, _tutorialGUI, _tutorialText1, _tutorialText2, _tutorialText3, _grass, _tutorialBubble;
	public static GameObject level, startLevel, heightText1, heightText2, heightGUI, tutorialGUI, tutorialText1, tutorialText2, tutorialText3, tutorialBubble;
	
	public GameObject _scoreText1, _scoreText2, _scoreLine;
	public static GameObject scoreText1, scoreText2;
	
	public GameObject _gameOverText1, _gameOverText2, _gameStartText;
	public static GameObject gameOverText1, gameOverText2;
	
	public static HighScoreScript hsScript;
	
	public Texture _login, _logout, _share;
	public static Texture login, logout, share;

	public GUITexture _loginoutButton, _shareButton;
	public static GUITexture loginoutButton, shareButton;
	
	public static GameObject lastLevel;
	
	public AudioClip _soundAchiUnlock, _soundBirdTweet, _soundBlip, _soundUFO, _soundWind;
	public static AudioClip soundAchiUnlock, soundBirdTweet, soundBlip, soundUFO, soundWind;
	
	public GameObject _soundAchiUnlockObject, _soundBlipObject, _soundMenuBlipObject, _soundWind1, _soundWind2;
	public static GameObject soundAchiUnlockObject, soundBlibObject, soundBlibMenuObject;
	
	// Use this for initialization
	void Awake () {
		Screen.orientation = ScreenOrientation.Portrait;
		
		GameEventManager.GameStart += GameStart;
		GameEventManager.GameOver += GameOver;
		GameEventManager.MainMenuStart += MainMenuStart;
		
		soundAchiUnlock = _soundAchiUnlock;
		soundBirdTweet = _soundBirdTweet;
		soundBlip = _soundBlip;
		soundUFO = _soundUFO;
		soundWind = _soundWind;
		
		soundAchiUnlockObject = _soundAchiUnlockObject;
		soundBlibObject = _soundBlipObject;
		soundBlibMenuObject = _soundMenuBlipObject;
		
		
		
		_grass.ScreenPlacement(ScreenPosition.LowerMiddle);
		_grass.transform.position = new Vector3(_grass.transform.position.x, 2, _grass.transform.position.z);
		
		tutorialGUI = _tutorialGUI;
		tutorialBubble = _tutorialBubble;
		tutorialText1 = _tutorialText1;
		tutorialText2 = _tutorialText2;
		tutorialText3 = _tutorialText3;
		
		startLevel = _startLevel;
		scoreText1 = _scoreText1;
		scoreText2 = _scoreText2;
		heightText1 = _heightText1;
		heightText2 = _heightText2;
		heightGUI = _heightGUI;
		gameOverText1 = _gameOverText1;
		gameOverText2 = _gameOverText2;
		level = _level;
		_scoreLine.GetComponent<Renderer>().enabled = true;
		_scoreText2.GetComponent<Renderer>().enabled = true;
		_scoreText1.GetComponent<Renderer>().enabled = true;
		hsScript = (HighScoreScript)_scoreGUI.GetComponent(typeof(HighScoreScript));
		
		login = _login;
		logout = _logout;
		share = _share;
		loginoutButton = _loginoutButton;
		shareButton = _shareButton;
		
		var loginWidth = Screen.width / 4f;
		var loginHeight = loginWidth * 0.33f;
		var shareWidth = loginWidth * 0.55f;
		loginoutButton.pixelInset = new Rect(-loginWidth - shareWidth - 4, -loginHeight - 10, loginWidth, loginHeight);
		shareButton.pixelInset = new Rect(-shareWidth - 10, -loginHeight - 10, shareWidth, loginHeight);
	}
	
	void Start(){
		_soundWind1.GetComponent<AudioSource>().enabled = enabled;
		_soundWind2.GetComponent<AudioSource>().enabled = enabled;
	}
	
	//Event Methods
	
	private void GameStart(){
		gameOverText1.GetComponent<Renderer>().enabled = false;
		gameOverText2.GetComponent<Renderer>().enabled = false;
		_gameStartText.GetComponent<Renderer>().enabled = false;
		foreach (GameObject obj in objectList) {
			Destroy(obj);
		}
		objectList.Clear();
		var sdf = (GameObject)Instantiate(_start, _start.transform.position, Quaternion.Euler(new Vector3(0, 180, 0)));
		startLevel = (GameObject)Instantiate(_startLevel, _startLevel.transform.position, _startLevel.transform.rotation);
		objectList.Add(sdf);
		objectList.Add(startLevel);
	}
	
	private void GameOver(){
		if (!_gameStartText.GetComponent<Renderer>().enabled){
			gameOverText1.GetComponent<Renderer>().enabled = true;
			gameOverText2.GetComponent<Renderer>().enabled = true;
		}
		_scoreLine.GetComponent<Renderer>().enabled = true;
		_scoreText2.GetComponent<Renderer>().enabled = true;
		_scoreText1.GetComponent<Renderer>().enabled = true;
	}
	
	private void MainMenuStart(){
		gameOverText1.GetComponent<Renderer>().enabled = false;
		gameOverText2.GetComponent<Renderer>().enabled = false;
	}
}
