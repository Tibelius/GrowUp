﻿using UnityEngine;
using System.Collections;

//
//		OPTIMIZED PARTLY!!!
//

public class HeightIndicatorScript : MonoBehaviour {
	
	// Update is called once per frame
	void FixedUpdate () {
		if (GameObjectManager.heightText1 != null){
			((TextMesh)GameObjectManager.heightText1.GetComponent("TextMesh")).text = transform.position.z.ToString("0.0");
		}
		if (GameObjectManager.heightText1 != null){
			float height1boundx = ((TextMesh)GameObjectManager.heightText1.GetComponent("TextMesh")).GetComponent<Renderer>().bounds.size.x;
			GameObjectManager.heightText2.transform.position = new Vector3(GameObjectManager.heightText1.transform.position.x + height1boundx, GameObjectManager.heightText1.transform.position.y, GameObjectManager.heightText1.transform.position.z - 0.2f);
			if (GameObjectManager.objectList.Count > 1){
				float posZ = GameObjectManager.objectList[GameObjectManager.objectList.Count - 1].transform.position.z;
				transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, posZ), Time.deltaTime * 2) ;
			}
		}
	}
}
