﻿using UnityEngine;
using System.Collections;

//
//		OPTIMIZED PARTLY!!!
//

public class LevelScript : MonoBehaviour {
	
	public int size;
	public bool clickable = true;
	public bool sizeBool = false;
	public bool rotBool = false;
	public float scaleAmount;
	public float moveAmount;
	public float time;
	public float time2;
	public float timeToClick;
	public float growRate;
	public float turnRate;
	public float relativeTurnRate;
	public float t1, t2;
	
	public float distanceToEnd;
	
	public Vector3 startRot;
	public Vector3 endRot;
	
	public GameObject texture1, texture2, texture3, texture4, textureGlow;
	public GameObject animation2, animation3, animation4;
	
	public int index;
	
	public Transform anchor;
	public Vector3 anchorposition;
	
	// Use this for initialization
	void Awake () {
		var renderers = GetComponentsInChildren<Renderer>();
		foreach (Renderer r in renderers) {
			r.enabled = false;
		}
		texture1.GetComponentInChildren<Renderer>().enabled = true;
		GetComponent<Renderer>().enabled = true;
		if (transform.parent != null){
		} else {
			startRot = new Vector3(0, 180, 0);
			endRot = new Vector3(0, 180, 0);
		}
	}
	
	void Start(){
		relativeTurnRate = 1;
		index = GameObjectManager.objectList.IndexOf(gameObject);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (clickable){
			textureGlow.GetComponentInChildren<Renderer>().enabled = true;
			if (size == 0){
			} else if (size == 1){
				animation2.GetComponentInChildren<Renderer>().enabled = true;
				texture2.GetComponentInChildren<Renderer>().enabled = false;
				if (texture1 != null){
					Destroy(texture1);
				}
			} else if (size == 2){
				animation3.GetComponentInChildren<Renderer>().enabled = true;
				texture3.GetComponentInChildren<Renderer>().enabled = false;
				if (texture2 != null){
					Destroy(texture2);
				}
				if (animation2 != null){
					Destroy(animation2);
				}
			} else {
				animation4.GetComponentInChildren<Renderer>().enabled = true;
				texture4.GetComponentInChildren<Renderer>().enabled = false;
				if (texture3 != null){
					Destroy(texture3);
				}
				if (animation3 != null){
					Destroy(animation3);
				}
			}
		} else {
			textureGlow.GetComponentInChildren<Renderer>().enabled = false;
			if (size == 0){
				texture1.GetComponentInChildren<Renderer>().enabled = true;
			} else if (size == 1){
				texture2.GetComponentInChildren<Renderer>().enabled = true;
				animation2.GetComponentInChildren<Renderer>().enabled = false;
				if (texture1 != null){
					Destroy(texture1);
				}
			} else if (size == 2){
				texture3.GetComponentInChildren<Renderer>().enabled = true;
				animation3.GetComponentInChildren<Renderer>().enabled = false;
				if (texture2 != null){
					Destroy(texture2);
				}
				if (animation2 != null){
					Destroy(animation2);
				}
			} else {
				texture4.GetComponentInChildren<Renderer>().enabled = true;
				animation4.GetComponentInChildren<Renderer>().enabled = false;
				if (texture3 != null){
					Destroy(texture3);
				}
				if (animation3 != null){
					Destroy(animation3);
				}
			}
		}
		
		IncreaseSize();
		ReverseAllowedSelectionByTime();
		RotateLevel();
		
		if (transform.position.z < -0.2f){
			GameEventManager.TriggerGameOver();
		}
	}
	
	public void RotateLevel(){
		if (rotBool){
			distanceToEnd = Vector3.Distance(transform.localEulerAngles, endRot);
			if (distanceToEnd > 360){
				distanceToEnd = distanceToEnd -360;
			}
			time2 += Time.deltaTime * ((distanceToEnd)  / turnRate);
	    	transform.localEulerAngles = Vector3.Lerp (startRot, endRot, time2);
			if (time2 >= 1 || distanceToEnd < 1){
				rotBool = false;
				time2 = 0;
			}
		}
	}
	
	private void IncreaseSize(){
		if (sizeBool){
			clickable = false;
			time += Time.deltaTime * growRate;
			
			transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(transform.localScale.x * scaleAmount, 1f, transform.localScale.z * scaleAmount), Time.deltaTime);
			
			if (transform.name.Contains("StartLevel")){
				transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, 0, 1) / moveAmount, Time.deltaTime);
			} else {
				transform.position = Vector3.Lerp(transform.position, transform.position + (-GameObjectManager.lastLevel.transform.forward / moveAmount), Time.deltaTime);
			}
			if (time >= 1){
				sizeBool = false;
				if (size != 4 && index == 1){
					clickable = true;
				}
				time = 0;
			}
		}
	}
	
	private void ReverseAllowedSelectionByTime(){
		if (clickable){
			if (index != 1 && GameObjectManager.objectList.Count > 2){
				timeToClick += Time.deltaTime;
				if (timeToClick >= 1.5){
					clickable = false;
					timeToClick = 0;
					((LevelScript)GameObjectManager.objectList[1].GetComponent(typeof(LevelScript))).clickable = true;
				}
			}
		}
	}
	
	public void ReverseAllowedSelection(){
		if (index == GameObjectManager.objectList.Count - 1 && GameObjectManager.objectList.Count > 2){
			clickable = false;
			((LevelScript)GameObjectManager.objectList[1].GetComponent(typeof(LevelScript))).clickable = true;
		}
	}
	
	void OnCollisionEnter(Collision col){
		GameEventManager.TriggerGameOver();
		GetComponent<Collider>().enabled = false;
	}
}
