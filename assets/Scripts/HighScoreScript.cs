﻿using UnityEngine;
using System.Collections;

//
//		OPTIMIZED PARTLY!!!
//

public class HighScoreScript : MonoBehaviour {
	
	public float highScore;
	public static float scorePositionZ;
	public float spos;
	
	// Use this for initialization
	void Start () {
		GameEventManager.GameStart += GameStart;
		GameEventManager.GameOver += GameOver;
		
		transform.ScreenPlacement(ScreenPosition.Right);
		if (PlayerPrefs.GetFloat("highscore") != 0){
			highScore = PlayerPrefs.GetFloat("highscore");
		} else { 
			highScore = 0;
		}
		transform.position = new Vector3(transform.position.x, 3, highScore);
	}
	
	// Update is called once per frame
	void Update () {
		spos = scorePositionZ;
		if (GameObjectManager.scoreText1 != null){
			((TextMesh)GameObjectManager.scoreText1.GetComponent("TextMesh")).text = highScore.ToString("0.00") + "m";
		}
	}
	
	public void PlaceHighScoreLine(){
		if (scorePositionZ > highScore){
			highScore = scorePositionZ;
			StoreHighscore(highScore);
			transform.position = new Vector3(transform.position.x, transform.position.y, scorePositionZ);
		}
	}
	
	void StoreHighscore(float newHighscore){
		float oldHighscore = PlayerPrefs.GetFloat("highscore"); 
		if(newHighscore > oldHighscore){
			PlayerPrefs.SetFloat("highscore", newHighscore);
		}
	}
	
	public void UpdateCurrentScore(){
		scorePositionZ = 0;
		foreach (GameObject item in GameObjectManager.objectList) {
		LevelScript script = ((LevelScript)item.GetComponent(typeof(LevelScript)));
			if (item.transform.position.z > scorePositionZ){
				scorePositionZ = item.transform.position.z;
				if (script.size == 2){
					scorePositionZ = scorePositionZ + 0.18f;
				} else if (script.size > 2){
					scorePositionZ = scorePositionZ + 0.3f;
				}
			}
		}
	}
	
	//Event Methods
	
	private void GameStart(){
		PlaceHighScoreLine();
	}
	
	private void GameOver(){
		PlaceHighScoreLine();
	}
}
