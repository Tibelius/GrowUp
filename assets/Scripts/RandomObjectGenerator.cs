﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//
//		OPTIMIZED PARTLY!!!
//

public class RandomObjectGenerator : MonoBehaviour {
	
	public float world1_2, world2_3, worldEnd;
	
	public float cloudStart, ufoStart;
	public int cloudMaxCount, ufoMaxCount, world1MaxCount, world2MaxCount, world3MaxCount;
	
	public int stationaryCount;
	public int spawnCount;
	
	public List<GameObject> world1Objects;
	public List<GameObject> world1Clouds;
	public List<GameObject> world2Objects;
	public List<GameObject> world3Objects;
	public List<GameObject> world3Ufo;
	
	public List<GameObject> world1ObjectsStationary;
	public List<GameObject> world2ObjectsStationary;
	public List<GameObject> world3ObjectsStationary;
	
	public float spawnTime;
	public float time;
	
	public GameObject stationaryContainer, movingContainer;
	
	public static List<GameObject> cloudList, ufoList, world1List, world2List, world3List;
	public List<GameObject> _cloudList, _ufoList, _world1List, _world2List, _world3List;
	
	public static List<Vector3> stationaryPositionList;
	public List<Vector3> _stationaryPositionList;
	public List<GameObject> spawnObjectList;
	
	private Transform cameratransform;
	
	// Use this for initialization
	void Start () {
		cameratransform = Camera.main.transform;
		if (stationaryCount != 0){
			for (int i = 0; i < stationaryCount; i++) {
				float spawnPointZ = Random.Range(world1_2, worldEnd);
				float spawnPointX = Random.Range(-5.5f, 5.5f);
				var usedList = new List<GameObject>();
				
				if (spawnPointZ > world1_2 && spawnPointZ <= world2_3){
					usedList = world2ObjectsStationary;
				} else if (spawnPointZ > world2_3){
					usedList = world3ObjectsStationary;
				}
			
				if (usedList.Count != 0){
					int index = Random.Range(0, usedList.Count);
					GameObject spawnObject = usedList[index];
					
					_stationaryPositionList.Add(new Vector3(spawnPointX, 0, spawnPointZ));
					spawnObjectList.Add(spawnObject);
					
				}
			}
		}
	}
	
	private void RespawnStationaryObject(){
		for (int i = 0; i < stationaryCount; i++) {
			if (_stationaryPositionList[i].z < cameratransform.position.z + 11 && _stationaryPositionList[i].z > cameratransform.position.z - 11 && _stationaryPositionList[i].y == 0){
				float spawnPointX = _stationaryPositionList[i].x;
				float spawnPointZ = _stationaryPositionList[i].z;
				_stationaryPositionList[i] = new Vector3(spawnPointX, 1, spawnPointZ);
				
				GameObject spawnObject = spawnObjectList[i];
				float spawnPointOffsetY = Random.Range(0f, 0.5f);
			
				GameObject obj = (GameObject)Instantiate(spawnObject, new Vector3(spawnPointX, spawnObject.transform.position.y + spawnPointOffsetY, spawnPointZ), spawnObject.transform.rotation);
				obj.transform.parent = stationaryContainer.transform;
				RandomObjectScript script = ((RandomObjectScript)obj.GetComponent(typeof(RandomObjectScript)));
				script.stationary = true;
				script.index = i;
				stationaryPositionList = _stationaryPositionList;
			}
		}
	}
	
	void FixedUpdate(){
		RespawnStationaryObject();
	}
	
	// Update is called once per frame
	void Update(){
		if (world1MaxCount + world2MaxCount + world3MaxCount > 0){
			time += Time.deltaTime;
		}
	}
	
	void LateUpdate () {
		if (world1MaxCount + world2MaxCount + world3MaxCount > 0){
		if (time > spawnTime){
			for (int i = 0; i < spawnCount; i++) {
				float spawnPointZ = Random.Range(5f, worldEnd);
				int spawnPointX = ((Random.Range(0, 2) * 2) - 1) * 9;
				float spawnPointOffsetY = Random.Range(0f, 0.5f);
				var usedList = new List<GameObject>();
				
				if (spawnPointZ <= world1_2  && Camera.main.transform.position.z <= world1_2){
					if (spawnPointZ > cloudStart && _cloudList.Count < cloudMaxCount ){
						usedList = world1Clouds;
					} else if (_world1List.Count < world1MaxCount) {
						usedList = world1Objects;
					} else {
						return;
					}
				} else if (spawnPointZ > world2_3 && Camera.main.transform.position.z > world2_3){
					if (spawnPointZ > ufoStart && _ufoList.Count < ufoMaxCount ){
						usedList = world3Ufo;
					} else if (_world3List.Count < world3MaxCount) {
						usedList = world3Objects;
					} else {
						return;
					}
				} else if ((spawnPointZ > world1_2 && spawnPointZ <= world2_3) && (Camera.main.transform.position.z > world1_2 && Camera.main.transform.position.z <= world2_3)){
					if (_world2List.Count < world2MaxCount) {
						usedList = world2Objects;
					} else {
						return;
					}
				} 
				
				if (usedList.Count != 0){
					int index = Random.Range(0, usedList.Count);
					GameObject spawnObject = usedList[index];
					
					GameObject obj = (GameObject)Instantiate(spawnObject, new Vector3(spawnPointX, spawnObject.transform.position.y + spawnPointOffsetY, spawnPointZ), spawnObject.transform.rotation);
					RandomObjectScript script = (RandomObjectScript)obj.GetComponent(typeof(RandomObjectScript));
					if (usedList == world1Clouds){
						script.listType = "world1Clouds";
						_cloudList.Add(obj);
					} else if (usedList == world3Ufo){
						script.listType = "world3Ufo";
						_ufoList.Add(obj);
					} else if (usedList == world1Objects){
						script.listType = "world1Objects";
						_world1List.Add(obj);
					} else if (usedList == world2Objects){
						script.listType = "world2Objects";
						_world2List.Add(obj);
					} else if (usedList == world3Objects){
						script.listType = "world3Objects";
						_world3List.Add(obj);
					} else {
						script.listType = "other";
					}
					obj.transform.parent = movingContainer.transform;
				}
			}
			world1List = _world1List;
			world2List = _world2List;
			world3List = _world3List;
			ufoList = _ufoList;
			cloudList = _cloudList;
			time = 0;
		}
		}
	}
}
