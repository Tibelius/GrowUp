﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//
//		OPTIMIZED PARTLY!!!
//

public class MenuButton : MonoBehaviour {
	
	public delegate void ButtonEvent();
	public event ButtonEvent Click, Click2;
	
	public GameObject activeTexture, inactiveTexture;
	public GameObject buttonText;
	
	public Renderer activeRenderer, inactiveRenderer;
	RaycastHit hit;
	
	public float width, height;
	public bool isActive = false;
	
	public bool clickable;
	
	public AudioClip soundMenuInput;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonUp(0) && clickable){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, 100)){
				if (hit.transform.gameObject == gameObject){
					if (!isActive){
						Active();
						TriggerClick();
						GameObjectManager.soundBlibMenuObject.GetComponent<AudioSource>().Play();
					} else {
						Inactive();
						TriggerClick2();
					}
				}
			}
		}
	}
	
	public void InitMenuButton(GameObject active, GameObject inactive, Vector3 position, string text, List<GameObject> list, bool isclickable = true, bool startsActive = false){
		((TextMesh)buttonText.GetComponent(typeof(TextMesh))).text = text;
		
		clickable = isclickable;
		
		activeTexture = (GameObject)Instantiate(active, new Vector3(position.x, position.y, position.z), active.transform.rotation);
		inactiveTexture = (GameObject)Instantiate(inactive, new Vector3(position.x, position.y, position.z), inactive.transform.rotation);
		
		activeRenderer = activeTexture.GetComponentInChildren<Renderer>();
		inactiveRenderer = inactiveTexture.GetComponentInChildren<Renderer>();
		
		width = activeRenderer.bounds.size.x;
		height = activeRenderer.bounds.size.z;
		
		transform.localScale = new Vector3(width, 1, height);
		
		if (startsActive){
			Active();
		} else {
			Inactive();
		}
		
		activeTexture.transform.parent = Camera.main.transform;
		inactiveTexture.transform.parent = Camera.main.transform;
		
		list.Add(activeTexture);
		list.Add(inactiveTexture);
	}
	
	public void Active(){
		activeRenderer.enabled = true;
		inactiveRenderer.enabled = false;
		isActive = true;
	}
	
	public void Inactive(){
		activeRenderer.enabled = false;
		inactiveRenderer.enabled = true;
		isActive = false;
	}
	
	public void TriggerClick(){
		if(Click != null){
			Click();
		}
	}
	
	public void TriggerClick2(){
		if(Click2 != null){
			Click2();
		}
	}
}
