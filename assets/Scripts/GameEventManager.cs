﻿using UnityEngine;
using System.Collections;

//
//		OPTIMIZED!!!
//

public static class GameEventManager {

	public delegate void GameEvent();
	
	public static event GameEvent GameStart, GameOver;
	public static event GameEvent MainMenuStart, MainMenuOver;
	public static event GameEvent AchiMenuStart, AchiMenuOver;
	
	public static void TriggerGameStart(){
		if (GameStart != null){
			GameStart();
		}
	}
	public static void TriggerGameOver(){
		if (GameOver != null){
			GameOver();
		}
	}
	
	public static void TriggerMainMenuStart(){
		if (MainMenuStart != null){
			MainMenuStart();
		}
	}
	public static void TriggerMainMenuOver(){
		if (MainMenuOver != null){
			MainMenuOver();
		}
	}
	
	public static void TriggerAchiMenuStart(){
		if (AchiMenuStart != null){
			AchiMenuStart();
		}
	}
	public static void TriggerAchiMenuOver(){
		if (AchiMenuOver != null){
			AchiMenuOver();
		}
	}
	
}
