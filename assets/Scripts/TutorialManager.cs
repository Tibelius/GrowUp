﻿using UnityEngine;
using System.Collections;

//
//		OPTIMIZED PARTLY!!!
//

public class TutorialManager : MonoBehaviour {
	
	public static bool tutorialCompleted;
	public static string[] stages = new string[8];
	public static int tutorialState;
	
	private const string Stage1Text3 = "Let's start!";
	private const string Stage1Text2 = "";
	private const string Stage1Text1 = "Click here to tilt left.";
	
	private const string Stage2Text3 = "Nice!";
	private const string Stage2Text2 = "Now click here";
	private const string Stage2Text1 = "to tilt right.";
	
	private const string Stage3Text3 = "Good job!";
	private const string Stage3Text2 = "After 2 clicks a";
	private const string Stage3Text1 = "new bird appears";
	
	private const string Stage4Text3 = "To avoid tilting";
	private const string Stage4Text2 = "click below";
	private const string Stage4Text1 = "or above the bird";
	
	private const string Stage5Text3 = "";
	private const string Stage5Text2 = "You can only click";
	private const string Stage5Text1 = "the glowing bird.";
	
	private const string Stage6Text3 = "Try to keep the";
	private const string Stage6Text2 = "tower straight and";
	private const string Stage6Text1 = "not hit the walls.";
	
	private const string Stage7Text3 = "You can scroll up by";
	private const string Stage7Text2 = "holding and dragging";
	private const string Stage7Text1 = "the background.";
	
	private const string Stage8Text3 = "";
	private const string Stage8Text2 = "Good luck";
	private const string Stage8Text1 = "and have fun!";
	
	private const float sizeOffset0 = -0.6f;
	private const float sizeOffset1 = -0.85f;
	private const float sizeOffset2 = -0.9f;
	private const float sizeOffset3 = -1.1f;
	
	private TextMesh line1, line2, line3;
	
	Transform clickableTransform;
	float sizeOffset;
	
	// Use this for initialization
	void Awake(){
		GameObjectManager.tutorialBubble.GetComponentInChildren<Renderer>().enabled = false;
		GameObjectManager.tutorialText1.GetComponent<Renderer>().enabled = false;
		GameObjectManager.tutorialText2.GetComponent<Renderer>().enabled = false;
		GameObjectManager.tutorialText3.GetComponent<Renderer>().enabled = false;
	}
	
	void Start () {
		GameEventManager.GameStart += GameStart;
		
		line1 = ((TextMesh)GameObjectManager.tutorialText1.GetComponent("TextMesh"));
		line2 = ((TextMesh)GameObjectManager.tutorialText2.GetComponent("TextMesh"));
		line3 = ((TextMesh)GameObjectManager.tutorialText3.GetComponent("TextMesh"));
		
		tutorialState = 0;
		stages[0] = "TutorialStage1";
		stages[1] = "TutorialStage2";
		stages[2] = "TutorialStage3";
		stages[3] = "TutorialStage4";
		stages[4] = "TutorialStage5";
		stages[5] = "TutorialStage6";
		stages[6] = "TutorialStage7";
		stages[7] = "TutorialStage8";
		UpdateStages();
		enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObjectManager.tutorialGUI != null && GameObjectManager.startLevel != null){
			PlaceTutorialGUI(tutorialState);
		}
		
		if (tutorialState >= stages.Length ){
			tutorialCompleted = true;
			GameObjectManager.tutorialBubble.GetComponentInChildren<Renderer>().enabled = false;
			GameObjectManager.tutorialText1.GetComponent<Renderer>().enabled = false;
			GameObjectManager.tutorialText2.GetComponent<Renderer>().enabled = false;
			GameObjectManager.tutorialText3.GetComponent<Renderer>().enabled = false;
			enabled = false;
		}
	}
	
	public static void SetStageBoolTrue(int stagenumber){
		if (stagenumber <= stages.Length){
			if (PlayerPrefs.GetInt(stages[stagenumber]) != 1){
				string stage = stages[stagenumber];
				PlayerPrefs.SetInt(stage, 1);
				UpdateStages();
			}
		}
	}
	
	public static void UpdateStages(){
		tutorialState = 0;
		for (int i = 0; i < stages.Length; i++) {
			string stageString = stages[i];
			var stageInt = PlayerPrefs.GetInt(stageString);
			tutorialState = tutorialState + stageInt;
		}
	}
	
	public static int CheckStage(){
		return tutorialState;
	}
	
	public static void IncreaseTemp(){
		tutorialState++;
	}
	
	private void PlaceTutorialGUI(int stage){
		if (GameObjectManager.objectList.Count != 0){
			foreach (GameObject item in GameObjectManager.objectList) {
				var script = ((LevelScript)item.GetComponent(typeof(LevelScript)));
				
				if (script != null){
					if (script.clickable){
						clickableTransform = item.transform;
						
						if (script.size == 0){
							sizeOffset = sizeOffset0;
						} else if (script.size == 1){
							sizeOffset = sizeOffset1;
						} else if (script.size == 2){
							sizeOffset = sizeOffset2;
						} else {
							sizeOffset = sizeOffset3;
						}
					}
				}
			}
			switch (stage) {
				case 0:
					PlaceText(Stage1Text1, Stage1Text2, Stage1Text3);
					break;
				case 1:
					PlaceText(Stage2Text1, Stage2Text2, Stage2Text3, -1);
					break;
				case 2:
					PlaceText(Stage3Text1, Stage3Text2, Stage3Text3, 1, 1);
					break;
				case 3:
					PlaceText(Stage4Text1, Stage4Text2, Stage4Text3, 1, 1);
					break;
				case 4:
					PlaceText(Stage5Text1, Stage5Text2, Stage5Text3, 1, 1);
					break;
				case 5:
					PlaceText(Stage6Text1, Stage6Text2, Stage6Text3, 1, 1);
					break;
				case 6:
					PlaceText(Stage7Text1, Stage7Text2, Stage7Text3, 1, 1);
					break;
				case 7:
					PlaceText(Stage8Text1, Stage8Text2, Stage8Text3, 1, 1);
					break;
			}
		}
	}
	
	private void PlaceText(string text1, string text2, string text3, int sizeOffsetMod = 1, float zOffset = 0){
		GameObjectManager.tutorialGUI.transform.position = new Vector3(clickableTransform.position.x, 4f, clickableTransform.position.z + zOffset) + (clickableTransform.right * (sizeOffset * sizeOffsetMod));
		line1.text = text1;
		line2.text = text2;
		line3.text = text3;
	}
	
	//Event Methods
	
	private void GameStart(){
		if (tutorialState <= stages.Length){
			enabled = true;
			GameObjectManager.tutorialBubble.GetComponentInChildren<Renderer>().enabled = true;
			GameObjectManager.tutorialText1.GetComponent<Renderer>().enabled = true;
			GameObjectManager.tutorialText2.GetComponent<Renderer>().enabled = true;
			GameObjectManager.tutorialText3.GetComponent<Renderer>().enabled = true;
		}
	}
}










