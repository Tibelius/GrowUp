﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//
//		OPTIMIZED PARTLY!!!
//

public class RandomObjectScript : MonoBehaviour {
	
	public float speed;
	public bool stationary;
	private float destroyPoint = 11;
	private float distanceToDestroy;
	public string listType;
	public int index;
	
	// Use this for initialization
	void Start () {
		if(listType == "world2Objects" || listType == "world3Objects" ){
			float rotationOffset = Random.Range(-10f, 10f);
			transform.Rotate(0, 0, rotationOffset);
		}
		if (!stationary){
			float speedOffset = Random.Range(0f, 0.5f);
			speed = speed + speedOffset;
			if (transform.position.x > 0){
				speed = -speed;
				destroyPoint = -destroyPoint;
				transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z );
			}
			GetComponent<Rigidbody>().velocity = Vector3.right * speed / 1.5f;
		}
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (!stationary){
//			transform.Translate(Vector3.right * speed / 50);
			distanceToDestroy = Vector2.Distance(new Vector2(transform.position.x, 0), new Vector2(destroyPoint, 0));
			if (distanceToDestroy <= 0.5){
				if (listType == "world1Clouds"){
					RandomObjectGenerator.cloudList.Remove(gameObject);
				} else if (listType == "world3Ufo"){
					RandomObjectGenerator.ufoList.Remove(gameObject);
				} else if(listType == "world1Objects"){
					RandomObjectGenerator.world1List.Remove(gameObject);
				} else if(listType == "world2Objects"){
					RandomObjectGenerator.world2List.Remove(gameObject);
				} else if(listType == "world3Objects"){
					RandomObjectGenerator.world3List.Remove(gameObject);
				} else if(listType == "other"){
					
				}
				Destroy(gameObject);
			}
		}
		if (stationary){
			if (transform.position.z < Camera.main.transform.position.z - 11 || transform.position.z > Camera.main.transform.position.z + 11){
				Destroy(gameObject);
				Vector3 position = RandomObjectGenerator.stationaryPositionList[index];
				position = new Vector3(position.x, 0, position.z);
				RandomObjectGenerator.stationaryPositionList[index] = position;
			}
		}
	}
}