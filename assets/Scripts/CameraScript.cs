﻿using UnityEngine;
using System.Collections;

//
//		OPTIMIZED PARTLY!!!
//

public class CameraScript : MonoBehaviour {
	
	public static GameObject target;
	public static bool gameIsOver;
	
	// Use this for initialization
	void Start () {
		GameEventManager.GameStart += GameStart;
		GameEventManager.GameOver += GameOver;
	}
	
	// Update is called once per frame
	void Update () {
		if (!Input.GetKey(KeyCode.Mouse0)){
			if (!gameIsOver){
				GetClickableLevelAsTarget();
			} 
			if (target != null && target.transform.position.z > 2){
				transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, target.transform.position.z + 5), Time.deltaTime * 3);
			} else {
				transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, 7), Time.deltaTime * 3);
			}
		}
	}
	
	private void GetClickableLevelAsTarget(){
		foreach (GameObject obj in GameObjectManager.objectList) {
			LevelScript script = (LevelScript)obj.GetComponent(typeof(LevelScript));
			if (script != null){
				if (script.clickable == true){
					target = obj;
				}
			}
		}
	}
	
	private void GetHighestLevelAsTarget(){
		float tempObjZ = 0;
		foreach (GameObject obj in GameObjectManager.objectList) {
			if (obj.transform.position.z > tempObjZ){
				tempObjZ = obj.transform.position.z;
				target = obj;
			}
		}
	}
	
	//Event Methods
	
	private void GameStart(){
		gameIsOver = false;
		GetClickableLevelAsTarget();
	}
	
	private void GameOver(){
		gameIsOver = true;
		target = GameObjectManager.heightGUI;
	}
}
