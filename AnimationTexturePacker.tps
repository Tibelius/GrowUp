<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.3.2</string>
        <key>fileName</key>
        <string>C:/Users/Tib90/Documents/Unity Projects/StackEm/AnimationTexturePacker.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity</string>
        <key>textureFileName</key>
        <filename>Assets/Resources/AnimationAtlas.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>8192</int>
            <key>height</key>
            <int>8192</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <true/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>Assets/Resources/AnimationAtlas_data.txt</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>Assets/Images/bird_01.png</filename>
            <filename>Assets/Images/bird_02.png</filename>
            <filename>Assets/Images/butterfly_01.png</filename>
            <filename>Assets/Images/butterfly_02.png</filename>
            <filename>Assets/Images/cloud_01.png</filename>
            <filename>Assets/Images/cloud_02.png</filename>
            <filename>Assets/Images/cloud_03.png</filename>
            <filename>Assets/Images/fly_01.png</filename>
            <filename>Assets/Images/fly_02.png</filename>
            <filename>Assets/Images/grass_01.png</filename>
            <filename>Assets/Images/grass_02.png</filename>
            <filename>Assets/Images/grass_03.png</filename>
            <filename>Assets/Images/grass_04.png</filename>
            <filename>Assets/Images/flower_01_01.png</filename>
            <filename>Assets/Images/flower_01_02.png</filename>
            <filename>Assets/Images/flower_01_03.png</filename>
            <filename>Assets/Images/flower_01_04.png</filename>
            <filename>Assets/Images/flower_02_01.png</filename>
            <filename>Assets/Images/flower_02_02.png</filename>
            <filename>Assets/Images/flower_02_03.png</filename>
            <filename>Assets/Images/flower_02_04.png</filename>
            <filename>Assets/Images/flower_03_01.png</filename>
            <filename>Assets/Images/flower_03_02.png</filename>
            <filename>Assets/Images/flower_03_03.png</filename>
            <filename>Assets/Images/flower_03_04.png</filename>
            <filename>Assets/Images/log_01.png</filename>
            <filename>Assets/Images/penguin_01_01.png</filename>
            <filename>Assets/Images/penguin_01_02.png</filename>
            <filename>Assets/Images/penguin_02_01.png</filename>
            <filename>Assets/Images/penguin_02_02.png</filename>
            <filename>Assets/Images/penguin_03_01.png</filename>
            <filename>Assets/Images/penguin_03_02.png</filename>
            <filename>Assets/Images/penguin_04_01.png</filename>
            <filename>Assets/Images/penguin_04_02.png</filename>
            <filename>Assets/Images/penguinGlow.png</filename>
            <filename>Assets/Images/bubble_01.png</filename>
            <filename>Assets/Images/star_01.png</filename>
            <filename>Assets/Images/star_02.png</filename>
            <filename>Assets/Images/star_03.png</filename>
            <filename>Assets/Images/star_04.png</filename>
            <filename>Assets/Images/starblue_01.png</filename>
            <filename>Assets/Images/starblue_02.png</filename>
            <filename>Assets/Images/starblue_03.png</filename>
            <filename>Assets/Images/starblue_04.png</filename>
            <filename>Assets/Images/starwhite_01.png</filename>
            <filename>Assets/Images/starwhite_02.png</filename>
            <filename>Assets/Images/starwhite_03.png</filename>
            <filename>Assets/Images/starwhite_04.png</filename>
            <filename>Assets/Images/menu_01.png</filename>
            <filename>Assets/Images/menu_02.png</filename>
            <filename>Assets/Images/meteor_01_01.png</filename>
            <filename>Assets/Images/meteor_01_02.png</filename>
            <filename>Assets/Images/meteor_01_03.png</filename>
            <filename>Assets/Images/meteor_01_04.png</filename>
            <filename>Assets/Images/meteor_02_01.png</filename>
            <filename>Assets/Images/meteor_02_02.png</filename>
            <filename>Assets/Images/meteor_02_03.png</filename>
            <filename>Assets/Images/meteor_02_04.png</filename>
            <filename>Assets/Images/menu_03.png</filename>
            <filename>Assets/Images/achi_01.png</filename>
            <filename>Assets/Images/achi_02.png</filename>
            <filename>Assets/Images/achi_03.png</filename>
            <filename>Assets/Images/achi_04.png</filename>
            <filename>Assets/Images/achi_05.png</filename>
            <filename>Assets/Images/achi_06.png</filename>
            <filename>Assets/Images/achi_07.png</filename>
            <filename>Assets/Images/achi_08.png</filename>
            <filename>Assets/Images/achi_09.png</filename>
            <filename>Assets/Images/achi_locked_01.png</filename>
            <filename>Assets/Images/ufo_01.png</filename>
            <filename>Assets/Images/ufo_02.png</filename>
            <filename>Assets/Images/ufo_03.png</filename>
            <filename>Assets/Images/ufo_04.png</filename>
            <filename>Assets/Images/ufo_05.png</filename>
            <filename>Assets/Images/ufo_06.png</filename>
            <filename>Assets/Images/achi_popup_long_01.png</filename>
            <filename>Assets/Images/achi_popup_long_01_02.png</filename>
            <filename>Assets/Images/achi_popup_long_01_03.png</filename>
            <filename>Assets/Images/achi_popup_long_01_04.png</filename>
            <filename>Assets/Images/achi_popup_long_02.png</filename>
            <filename>Assets/Images/achi_popup_long_02_02.png</filename>
            <filename>Assets/Images/achi_popup_long_02_03.png</filename>
            <filename>Assets/Images/achi_popup_long_02_04.png</filename>
            <filename>Assets/Images/achi_popup_long_03.png</filename>
            <filename>Assets/Images/achi_popup_long_03_02.png</filename>
            <filename>Assets/Images/achi_popup_long_03_03.png</filename>
            <filename>Assets/Images/achi_popup_long_03_04.png</filename>
            <filename>Assets/Images/achi_popup_long_04.png</filename>
            <filename>Assets/Images/achi_popup_long_04_02.png</filename>
            <filename>Assets/Images/achi_popup_long_04_03.png</filename>
            <filename>Assets/Images/achi_popup_long_04_04.png</filename>
            <filename>Assets/Images/achi_popup_long_05.png</filename>
            <filename>Assets/Images/achi_popup_long_05_02.png</filename>
            <filename>Assets/Images/achi_popup_long_05_03.png</filename>
            <filename>Assets/Images/achi_popup_long_05_04.png</filename>
            <filename>Assets/Images/achi_popup_long_06.png</filename>
            <filename>Assets/Images/achi_popup_long_06_02.png</filename>
            <filename>Assets/Images/achi_popup_long_06_03.png</filename>
            <filename>Assets/Images/achi_popup_long_06_04.png</filename>
            <filename>Assets/Images/achi_popup_long_07.png</filename>
            <filename>Assets/Images/achi_popup_long_07_02.png</filename>
            <filename>Assets/Images/achi_popup_long_07_03.png</filename>
            <filename>Assets/Images/achi_popup_long_07_04.png</filename>
            <filename>Assets/Images/achi_popup_long_08.png</filename>
            <filename>Assets/Images/achi_popup_long_08_02.png</filename>
            <filename>Assets/Images/achi_popup_long_08_03.png</filename>
            <filename>Assets/Images/achi_popup_long_08_04.png</filename>
            <filename>Assets/Images/achi_popup_long_09.png</filename>
            <filename>Assets/Images/achi_popup_long_09_02.png</filename>
            <filename>Assets/Images/achi_popup_long_09_03.png</filename>
            <filename>Assets/Images/achi_popup_long_09_04.png</filename>
            <filename>Assets/Images/sound_02.png</filename>
            <filename>Assets/Images/sound_01.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
